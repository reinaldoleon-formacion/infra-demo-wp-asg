############
## REGION ##
############
variable "region" {
  default = "eu-west-1"
}
##########################################################
## DEFINE THE NAME APROVISIONET FOR CERTIFICATE MANAGER ##
##########################################################
variable "domain_name" {
  default   =   "devopsgeekshubsacademy.click"  ##CAMBIAR
}

variable "subject_alternative_names" {
  default   =   [
      "*.devopsgeekshubsacademy.click", ## CAMBIAR
      "devopsgeekshubsacademy.click"    ## CAMBIAR
  ]
}
variable "validation_method" {
  default   =   "DNS"
}
##################
## DNS ROUTE 53 ##
##################
variable "dns_zone_name" {
  default   =   "devopsgeekshubsacademy.click."  ## IMPORTANT THE FINAL POINT ## CAMBIAR
}
variable "private_zone" {
  default   =   "false"
}

